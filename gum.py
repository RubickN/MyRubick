from pandas import DataFrame
import os
import sys
import re
import time


def get_result_path():
    path = os.path.dirname(__file__) + '/result/'
    if os.path.exists(path) is False:
        os.mkdir(path)
    return path


def get_all_time_data():
    result_path = get_result_path()
    time_pattern = re.compile("(\d+-\d+-\d+)")
    all_time = []
    result = {}
    for each_file in os.listdir(result_path):
        m = time_pattern.search(each_file.decode('GB2312'))
        if m:
            all_time.append("-".join(str(m.groups()[0]).split("-")[0:2]))
    return all_time
    # for i in all_time:
    #     result[i] = all_time.count(i)


def analyze_every_mouth_count():
    all_time = get_all_time_data()
    result = {}
    for i in all_time:
        result[i] = all_time.count(i)
    return result


def analyze_every_year_count():
    all_time = analyze_every_mouth_count()
    result = {}
    for i in all_time:
        strs = i.split("-")
        year = strs[0]
        if year in result:
            result[year] += all_time[i]
        else:
            result[year] = 0
    return result


def total_count():
    all_time = get_all_time_data()
    return len(all_time)


def analyze_every_year():
    all_times = analyze_every_mouth_count().keys()
    result = {}
    for i in all_times:
        strs = i.split("-")
        year = strs[0]
        mouth = strs[1]
        if year in result:
            if mouth not in result[year]:
                result[year].append(mouth)
        else:
            result[year] = []

    tmp = result.values()[0]
    for i in result.values():
        tmp = set(i) & set(tmp)
    return list(tmp)


if __name__ == "__main__":
    # analyze_every_mouth_count()
    # print total_count()
    # print analyze_every_year()
    analyze_every_year_count()